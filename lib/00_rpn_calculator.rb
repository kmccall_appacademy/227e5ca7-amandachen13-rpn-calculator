class RPNCalculator
  # TODO: your code goes here!
  def initialize
    @stack = []
    @length = 0
  end

  def push(num)
    @stack.push(num)
    @length += 1
  end

  def check
    if @length == 0
      raise "calculator is empty"
    end
  end

  def plus
    self.check
    popped = @stack.pop
    @stack[-1] += popped
    @length -= 1
  end

  def minus
    self.check
    popped = @stack.pop
    @stack[-1] -= popped
    @length -= 1
  end

  def times
    self.check
    popped = @stack.pop
    @stack[-1] *= popped
    @length -= 1
  end

  def divide
    self.check
    popped = @stack.pop
    @stack[-1] = @stack[-1].fdiv(popped)
    @length -= 1
  end

  def value
    @stack[-1]
  end

  def tokens(tokens_str)
    operations = ["+", "-", "*", "/"]

    tokens_str.split.map do |token|
      if operations.include?(token)
        token.to_sym
      else
        token.to_i
      end
    end
  end

  def evaluate(tokens_str)
    self.tokens(tokens_str).each do |token|
      if token == :+
        self.plus
      elsif token == :-
        self.minus
      elsif token == :*
        self.times
      elsif token == :/
        self.divide
      else
        self.push(token)
      end
    end
    self.value
  end

end
